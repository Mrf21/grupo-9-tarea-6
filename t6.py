varA="hola"
varB=55
a=isinstance(varA,str)
b=isinstance(varB,str)
if a==1 and b==1:
    print("String involucrado\n")
    if len(varA)>len(varB):
        print("Más grande\n")
    if len(varA)<len(varB):
        print("Más pequeño\n")
    if len(varA)==len(varB):
        print("Igual\n")
if a==1 and b==0:
    if len(varA)>varB:
        print("Más grande\n")
    if len(varA)<varB:
        print("Más pequeño\n")
    if len(varA)==varB:
        print("Igual\n")
if a==0 and b==1:
    if varA>len(varB):
        print("Más grande\n")
    if varA<len(varB):
        print("Más pequeño\n")
    if varA==len(varB):
        print("Igual")
if a==0 and b==0:
    if varA>varB:
        print("Más grande\n")
    if varA<varB:
        print("Más pequeño\n")
    if varA==varB:
        print("Igual\n")

#ejercicio 3 parte 1
print("\nEjercicio 3")
#Ciclo while
print('While loop')
i=1
while i<15:
    if i%2==0: #se comprueba que sea divisible por 2
        print(i)
    i+=1 #se suma 1 a i para pasar a la siguiente iteración
print("Adios!\n")
#ejercicio 3 parte 2
#ciclo for
print('For loop')
print("Hello") #se imprime hello fuera del loop
for i in range(10,0,-1): #se parte esta vez en 10 y se va disminuyendo de a 1 hasta llegar a 0 
    if i%2==0:
        print(i)


#ejercicio 4
print("\nEjercicio 4\n")
final=int(input("Escriba el número final del contador: "))
i=1
suma=0
for i in range(1,final+1): #ya que parte de 1, se utiliza límite superior como final+1
    suma+=i #se suman los valores de “i” en cada ciclo
    if i<(final): #si “i” es menor al número entregado por usuario
        print(i,'+ ',end='')
    if i==(final): #si “i” es igual al número entregado por usuario (final)
        print(i,end=' ')
print("=",suma)
